def decorator_function(func):
    def wrapper_function(*args,  **kwargs):
        print('Inside Wrapper')
        func(*args,  **kwargs)
    return wrapper_function

@decorator_function
def f1():
    print('this is f1')

@decorator_function
def f3(a):
    print(a)

f1()
f3('F3 param')